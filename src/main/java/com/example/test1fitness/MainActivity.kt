package com.example.test1fitness

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val addExercise = findViewById<ImageButton>(R.id.btnadd)
        val searchExercise = findViewById<EditText>(R.id.etSearch_Exercise)
        var addedWorkout = findViewById<TextView>(R.id.selected_exercise)

        addExercise.setOnClickListener{
            val workout = searchExercise.text.toString()
            addedWorkout.text = workout
            searchExercise.text.clear()


        }



    }
}